package top.zlhy7.constant;

/**
 * @author 沙福林
 * @date 2023/9/27 下午8:19
 * @description 常量
 */
public interface Constants {
    /**
     * 文件分隔符
     */
    String FILE_SEPARATOR = "/";
}
